create database  bookies;
use bookies;
CREATE TABLE books(
  book_id        INT NOT NULL, 
  title          VARCHAR(255) NOT NULL, 
  total_pages    INT NULL, 
  rating         DECIMAL(4, 2) NULL, 
  isbn           int not NULL, 
  PRIMARY KEY(book_id)
);

INSERT INTO BOOKS VALUES(1,"The gold man and the sea",300,4.5,145415),
(2,"To kill a mockingbird",450,4.5,452451),
(3,"Lord of the flies",350,3.5,145451),
(4,"The very hungry cat",250,4.6,142451),
(5,"Harrypotter",600,4.1,152451),
(6,"The wind in windows",450,4.2,222451),
(7,"The giver",220,4.3,111451),
(8,"The gruffalo",340,4.4,456451),
(9,"The giving tree",680,4.6,145789),
(10,"jany eyre",740,4.5,452151);

select * from books;
create table users (
   user_id 	   int not null auto_increment,
   name        varchar(45) not null,
   email 	   varchar(45) not null,
   password    varchar(45) not null,
   primary key(user_id)
);

insert into users values(1,"admin","admin@gmail.com","123");

select * from users;
